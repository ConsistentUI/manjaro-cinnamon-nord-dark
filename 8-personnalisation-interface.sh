#!/bin/bash
#
echo "----------------------------------------------------"
echo "    Personnalisations de l'interface     "
echo "                  Customize GUI                    "
echo "----------------------------------------------------"
gsettings set org.gnome.desktop.interface cursor-theme "Bibata-Modern-Classic"
gsettings set org.gnome.desktop.interface icon-theme "Colloid-nord-dark"
gsettings set org.gnome.desktop.wm.preferences num-workspaces 2
gsettings set org.gnome.desktop.interface gtk-theme 'Adwaita-dark'
gsettings set org.gnome.desktop.peripherals.touchpad natural-scroll true
gsettings set org.gnome.system.location enabled true
gsettings set org.gnome.desktop.datetime automatic-timezone true
gsettings set org.gnome.desktop.calendar show-weekdate true
gsettings set org.gnome.desktop.interface color-scheme 'prefer-dark'
gsettings set org.gnome.desktop.wm.preferences button-layout 'close:menu,minimize'
gsettings set org.cinnamon.desktop.wm.preferences button-layout 'close:menu,minimize'
gsettings set org.gnome.Terminal.Legacy.Settings default-show-menubar false
