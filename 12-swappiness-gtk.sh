#!/bin/bash
#
echo "-------------------------------------------------"
echo "             Swappiness à 10%                "
echo "     Change swappiness to 10 %       "
echo "-------------------------------------------------"
# Meilleure gestion de mémoire supérieure à 16Go
sudo tee /etc/sysctl.d/swappiness.conf <<< vm.swappiness=10
