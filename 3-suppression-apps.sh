#!/bin/bash
#
echo "--------------------------------------------------------------------"
echo "    Suppression d'applications non souhaitées     "
echo "              Remove unwanted applications               "
echo "--------------------------------------------------------------------"

sudo pacman -Runs cups timeshift lollypop samba pidgin gnome-terminal xreader xviewer vivaldi microsoft-office-web-jak parcellite galculator pix celluloid snapd flatpak
