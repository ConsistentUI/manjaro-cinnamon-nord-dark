#!/bin/bash
#
echo "-----------------------------------------------"
echo "       Activation de fstrim.timer        "
echo "      Enable fstrim.timer for SSD      "
echo "-----------------------------------------------"
# fstrim.timer
sudo systemctl enable fstrim.timer
