#!/bin/bash
#
echo "---------------------------------------------------"
echo "        Applications du dépôt AUR        "
echo "    Install apps from AUR repository   "
echo "---------------------------------------------------"
yay -S debhelper dh-autoreconf nemo-media-columns colloid-gtk-theme-git colloid-icon-theme-git google-chrome elementary-store-gabutdm-git spotify-edge preload ttf-ms-fonts mkinitcpio-firmware
