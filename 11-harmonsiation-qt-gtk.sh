#!/bin/bash
#
echo "----------------------------------------------------------------------------"
echo "    Harmoniser  les applications Qt et GTK via qt6ct       "
echo "     Make Qt and GTK apps look the same via qt6ct        "
echo "----------------------------------------------------------------------------"
# Pour que les applications KDE utilisent qt6ct
echo "QT_QPA_PLATFORMTHEME=qt6ct" >> /etc/environment
