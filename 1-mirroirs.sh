#!/bin/bash
#
echo "------------------------------------------------"
echo "   Choix du miroir le plus rapide     "
echo " Choose faster mirror for pacman "
echo "------------------------------------------------"
sudo pacman-mirrors --fasttrack
sudo pamac update --force-refresh --no-confirm
sudo pacman -Syu
