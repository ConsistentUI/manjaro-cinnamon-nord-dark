#!/bin/bash
#
echo "------------------------------------------------------------"
echo "    Pincer pour zoomer sur Firefox Xorg        "
echo "    Pinch to zoom on Firefox using Xorg        "
echo "------------------------------------------------------------"
echo export MOZ_USE_XINPUT2=1 | sudo tee /etc/profile.d/use-xinput2.sh
