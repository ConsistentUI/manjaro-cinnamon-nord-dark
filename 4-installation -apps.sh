#!/bin/bash
#
echo "----------------------------------------------------------"
echo "               Applications  du dépôt                "
echo " Install some apps from main repository  "
echo "---------------------------------------------------------"

sudo pacman -S base-devel git autoconf automake manjaro-starter dconf-editor onlyoffice-desktopeditors libgexiv2 bulky patch autoconf make gthumb automake bootsplash-manager bootsplash-systemd gedit brave-browser gnome-calculator mpv gcolor3 evince adw-gtk3 cinnamon-remove-application exiv2 ffmpeg firefox thunderbird ffmpegthumbnailer gnome-terminal-fedora manjaro-pipewire wireplumber sassc gradience qt6ct adwaita-qt5 adwaita-qt6 gtk-engine-murrine

# Nettoyage des anciennes versions
# Clean pacman
pamac clean --no-confirm
