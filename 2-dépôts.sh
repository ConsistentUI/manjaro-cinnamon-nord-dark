#!/bin/bash
#
echo "-----------------------------------------"
echo "   Configuration des dépôts   "
echo "      Repositories settings        "
echo "-----------------------------------------"
sudo sed -Ei '/NoUpdateHideIcon/s/^#//' /etc/pamac.conf
sudo sed -Ei '/DownloadUpdates/s/^#//' /etc/pamac.conf
sudo sed -Ei '/EnableAUR/s/^#//' /etc/pamac.conf
sudo sed -Ei '/CheckAURUpdates/s/^#//' /etc/pamac.conf
